import subprocess
import os, sys


#cmd = ['gnome-terminal'] # Se estiver usando o GNOME
#subprocess.Popen(cmd, stdout=subprocess.PIPE)

try:

	print(" ---------- Executando o build prod  ---------- ")
	os.system("ng build --prod")

	x = input("Continuar execucao (1-Sim) (0-Nao) >>:")
	if x is '0':
		sys.exit()		

	print(" ---------- Copiando os arquivos ---------- ")
	os.system("robocopy C:\/Users\/Adriano\/Dropbox\/dataBOX\/PROJETOS\/Em_andamento\/[PLATAFORMA_SATS]\/Desenvolvimento_TI\/Projeto\/dataBox\/dist C:\/Users\/Adriano\/Dropbox\/dataBOX\/PROJETOS\/Em_andamento\/[PLATAFORMA_SATS]\/Desenvolvimento_TI\/Projeto\/prod_geral\/micsbox /e")

	print(" ---------- Mudando de pasta  ---------- ")
	os.chdir("C:\/Users\/Adriano\/Dropbox\/dataBOX\/PROJETOS\/Em_andamento\/[PLATAFORMA_SATS]\/Desenvolvimento_TI\/Projeto\/prod_geral\/micsbox")

	print(" ---------- Comandos GIT  ---------- ")
	print(" ---------- Git Add  ---------- ")
	os.system("git add -A")

	print(" ---------- Git Add  ---------- ")
	os.system("git add -A")

	print(" ---------- Git commit ---------- ")
	name = input("Texto do commit >>:\n")
	os.system("git commit -m \""+ name+ "\" ")

	print(" ---------- Git PUSH ---------- ")
	os.system("git push -u origin master")

except:
	print("Ops ocorreu um erro durante a execução de algum programa :(") 
	sys.exit()

print(" ---------- Fim da execução :) ----------")