import { DataBoxPage } from './app.po';

describe('data-box App', () => {
  let page: DataBoxPage;

  beforeEach(() => {
    page = new DataBoxPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
