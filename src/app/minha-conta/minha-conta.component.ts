import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { toast } from 'angular2-materialize';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Usuario } from '../usuario';
import { Links } from './../links';
import { AuthService } from './../login/auth.service';
import { Strings } from '../strings';

@Component({
  selector: 'app-usuarios',
  templateUrl: './minha-conta.component.html',
  styleUrls: ['./minha-conta.component.css']
})

export class MinhaContaComponent implements OnInit, OnDestroy {

  usuario: Usuario = new Usuario();
  links: Links = new Links();

  //[(ngModel)]
  senhaAtual: String = "";
  novaSenha: String = "";
  novaSenhaConfirmacao: String = "";

  mostrarSenha: boolean = false;

  url: String;
  uploader: FileUploader;

  constructor(private router: Router, private authService: AuthService, private http: HttpClient) {
  }

  ngOnInit() {

    this.authService.usuarioEmitter.subscribe(
      user => this.usuario = user
    );

    this.usuario = this.authService.getUsuario();
    this.url = this.usuario.linkImageUser;

    //criando o objeto que envia a imagem para o servidor 
    this.uploader = new FileUploader({ url: this.usuario.uploaderImage });// 1

    // mudando o nome da imagem antes de enviá-la e tirarando a necessidade de credenciais 
    this.uploader.onBeforeUploadItem = (item) => {

      let nome = this.usuario.nameImageUser.split("_");
      let vec = nome[2].split(".");
      let cont: number = Number(vec[0]);

      cont += 1;

      //console.log("cont >>:"+cont);//////////////////////

      var fileExtension = '.' + item.file.name.split('.').pop();
      item.file.name = this.usuario.idUser + "_" + this.usuario.idLojista + "_" + cont + fileExtension;
      //console.log("item.file.name >>:"+item.file.name);//////////////////////

      item.withCredentials = false;
    }

    //aqui criar uma lógica para se der errado avisar o usuario e deixar ele enviar novamente   
    this.uploader.onCompleteItem = (item, response, status, header) => {
      if (status === 200) {
        //console.log("resposta do servidor >>:" + response); //resposta que veio do servidor
        //console.log("item >>:" + )
        this.authService.atualizarDadosUsuario();
      }
    }

  }

  ngOnDestroy() {
  }

  mudarImagem(event) { //metodo chamado quando o usuario clica na imagem dele  
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.url = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  mostrarEsconderSenha() {
    this.mostrarSenha = !this.mostrarSenha;
  }


  onSubmit(form) { //metodo chamado quando o usuario clica no botao para alterar os dados
    let u: any = {};
    u.senhaAtual = form['value']['senhaAntiga'];
    u.new_senha = form['value']['senhaNova'];
    u.user_id = this.usuario.idUser;

    let senha = localStorage.getItem("PlataformaDataBoxSenha");

    if (u.senhaAtual != senha) {
      toast(Strings.msgInvalidPassAtual, 2000);
    } else {
      this.enviarRequisicao(u);
    }
  }

  enviarRequisicao(u) {
    this.http.post(this.links.lUpdateSenha, u, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe((data) => {
      //console.log(data);
      if (data['id'] == 0) { // houve um problema 
        toast(Strings.msgErro, 2000);
      } else {
        toast(Strings.msgPasswordAltSuccess, 2000);
        localStorage.setItem("PlataformaDataBoxSenha", u.new_senha);
        this.router.navigate(['/cashback']); //mudando a rota
      }
    });
  }

  cancelar() {
    this.router.navigate(['/cashback']);
  }

}
