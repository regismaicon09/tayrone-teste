import { Component, OnInit, OnDestroy } from '@angular/core';
import { toast } from 'angular2-materialize';
import { Subscription } from 'rxjs/Rx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../../login/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Usuario } from './../../usuario';
import { Links } from './../../links';

import { ClientesService } from './../clientes-service.service';
import { IFormCanDesactivate } from "../../guards/i-form-candesactivate";
import { Cliente } from '../cliente';
import { Strings } from '../../strings';

@Component({
  selector: 'app-novo-cliente',
  templateUrl: './novo-cliente.component.html',
  styleUrls: ['./novo-cliente.component.css']
})

export class NovoClienteComponent implements OnInit, IFormCanDesactivate {
  

  user: Usuario = new Usuario();
  links: Links = new Links();

  formMudou: boolean = false;
  mostrarPreloader: boolean = false;
  mostrarPreloaderCL: boolean = false;
  clienteEncontrado: boolean = false;
  clienteJaCadastrado: boolean = false;

  cliente: Cliente = new Cliente();

  public maskCep = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  public maskcel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  // Acesso Modulos 
colaborador: any = {};

// id_colabordor: number;
// nome_colaborador: string;
// funcao_colaborador: string;

//Permiçoes
mostrarPermissoes: boolean = false;

//Requisiçoes
cashModule: boolean = false; configCampanha: boolean = false; clientsModule: boolean = false; 
configModule: boolean = false; powerbiModule: boolean = false; configRelatorio: boolean = false;

//switchs
check_cash: string; check_campanha: string; check_clientes: string; check_config: string; 
check_powerbi: string; check_relatorios: string;
///
  //[(ngModel)]
  cnpjClient: string = "";
  email: string = "";
  emailConfirm: string = "";
  dadosEndereco: any;
  cpfConsult: string;

  constructor(private router: Router, private authService: AuthService, private http: HttpClient, private location: Location) { }

  ngOnInit() {
    this.user = this.authService.getUsuario();
  }

  ngOnDestroy() {
  }

  onInput() { //chamando quando o usuário digita algo em um dos campos
    this.formMudou = true;
  }

  // buscarCliente(cpf) { //metodo chamado quando o usuario preenche o campo de cpf, o sistema busca os dados do cliente com esse cpf
  //   this.formMudou = true;
  //   this.clienteEncontrado = false;
  //   this.clienteJaCadastrado = false;
  //   if (cpf['valid'] == true) {
  //     let dados: any = {};
  //     dados.cpf = String(cpf['value']);
  //     dados.company_id = this.user.idLojista;
  //     this.mostrarPreloaderCL = true;
  //     //console.log("dados enviados >>:" + dados); //////////////////////////
  //     this.http.post(this.links.lVerificaCPF, dados, {
  //       headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  //     }).subscribe((data: Cliente) => {
  //       //console.log(data);//////////////////////
  //       this.mostrarPreloaderCL = false;
  //       if (data.id == 1) { //cliente encontrado, falta vincular 
  //         this.cliente = data;
  //         this.clienteEncontrado = true;
  //         this.clienteJaCadastrado = false;
  //         this.cpfConsult = dados.cpf;
  //       } else if (data.id == 0) { //cliente nao encontrado 
  //         this.clienteEncontrado = false;
  //         this.clienteJaCadastrado = false;
  //       } else if (data.id == 2) { //cliente ja cadastrado e vinculado a empresa 
  //         this.cliente = data;
  //         this.clienteEncontrado = false;
  //         this.clienteJaCadastrado = true;
  //         this.formMudou = false; //para que possa sair da pagina sem perguntar se realmente quer mudar de pagina 
  //       }
  //     });
  //   }
  // }

  zerardados() {
    this.formMudou = false; //para que se permita mudar de rota    
    // this.cnpjClient = "";--------------------------------------------FALTA
    this.email = "";
    this.emailConfirm = "";
    this.clienteEncontrado = false;
    this.cliente = null;
  }

  buscarCep(cep) {
    if (cep['valid'] == true) {
      let c = String(cep['value']);
      c = c.replace(".", "");
      c = c.replace("-", "");
      // console.log(c);//////////////////////

      this.http.get(`http://viacep.com.br/ws/${c}/json`)
        .subscribe(data => {
          //console.log("endereco >>:" + JSON.stringify(data)); ////////////////// 
          this.dadosEndereco = data;
        });
    }
  }

  setDados() {
    //console.log("entrou no setChecked()");///////////////////////////
  
    // this.nome_colaborador = this.colaborador.nome;
    // this.funcao_colaborador = this.colaborador.funcao;
  
    this.cashModule = this.colaborador.cashModule;
    this.configCampanha = this.colaborador.configCampanha;
    this.clientsModule = this.colaborador.clientsModule;
    this.configModule = this.colaborador.configModule;
    this.powerbiModule = this.colaborador.powerbiModule;
    this.configRelatorio = this.colaborador.configRelatorio;
  
    if (this.cashModule) {
      this.check_cash = "checked";
    } else {
      this.check_cash = "";
    }
  
    if (this.configCampanha) {
      this.check_campanha = "checked";
    } else {
      this.check_campanha = "";
    }
  
    if (this.clientsModule) {
      this.check_clientes = "checked";
    } else {
      this.check_clientes = "";
    }
  
    if (this.configModule) {
      this.check_config = "checked";
    } else {
      this.check_config = "";
    }
  
    if (this.powerbiModule) {
      this.check_powerbi = "checked";
    } else {
      this.check_powerbi = "";
    }
  
    if (this.configRelatorio) {
      this.check_relatorios = "checked";
    } else {
      this.check_relatorios = "";
    }
  
    //this.mostrarPreloader = false;
  }
  
  //chamado quando o usuario clica em algum checkbox
  mudarFlagPermissao(tipo) {
  
    switch (tipo) {
      case "cash":
        this.cashModule = !this.cashModule;
        break;
  
      case "cam":
        this.configCampanha = !this.configCampanha;
        break;
  
      case "cli":
        this.clientsModule = !this.clientsModule;
        break;
  
  
      case "conf":
        this.configModule = !this.configModule;
        break;
  
      case "power":
        this.powerbiModule = !this.powerbiModule;
        break;
  
      case "rel":
        this.configRelatorio = !this.configRelatorio;
        break;
    }
  
  }

    //chamado quando o usuário clica no botão salvar
  onSubmit(form) {
    
      //console.log(this.cliente);   ///////  
      let dados: any = {};
      dados.cnpj= form['value']['cnpj'];
      dados.nome = form['value']['nome'];
      dados.celular = form['value']['celular'];
      dados.telefone = form['value']['telefone'];
      dados.cep = form['value']['cep'];
      dados.numero = form['value']['numero'];
      dados.complemento = form['value']['complemento'];
      dados.logradouro = form['value']['logradouro'];
      dados.bairro = form['value']['bairro'];
      dados.cidade = form['value']['cidade'];
      dados.estado = form['value']['estado'];
      dados.cashModule = Number(this.cashModule);
      dados.configCampanha = Number(this.configCampanha);
      dados.clientsModule = Number(this.clientsModule);
      dados.configModule = Number(this.configModule);
      dados.powerbiModule = Number(this.powerbiModule);
      dados.configRelatorio = Number(this.configRelatorio);

  
    this.mostrarPreloader = true;

    console.log("dados enviados novo-cliente>>:" + JSON.stringify(dados)); //////////////////////////
    this.http.post(this.links.lcadEmpresa, dados, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe(data => {
      console.log(data);//////////////////////
      this.mostrarPreloader = false;
      if (data["id"] == 0) { // ocorreu um erro 
        //console.log("error >>:" + data["msg"]);//////////////////////
        toast(Strings.msgErro, 3000);
      } else if (data["id"] == 2) {
        console.log("error >>:" + data["msg"]);//////////////////////
        toast(Strings.msgClientExist, 3000);
        this.zerardados();
      }
      else {
       // this.zerardados();
        toast(Strings.msgCadSuccess, 3000);
        //console.log("error >>:" + data["msg"]);//////////////////////
      }
    });

  }
  mostrarEsconderPermissoes() {
    this.mostrarPermissoes = !this.mostrarPermissoes;
  }

  //caso o usuário altere alguma coisa no formulário, perguntar se ele realmente quer sair 
  //metodo da interface IFormCanDesactivate
  podeMudarRota() {
    if (this.formMudou) {
      if (confirm("Tem certeza que deseja sair dessa página?\nAs alterações serão perdidas.")) {
        return true;
      } else {
        return false;
      }
    }
    else {
      return true;
    }
  }

  cancelar() { //chamado quando o usuário clica no botão cancelar 
    //toast("Operação cancelada", 3000);
    this.formMudou = false;
    //this.router.navigate();
    this.location.back();
  }

}