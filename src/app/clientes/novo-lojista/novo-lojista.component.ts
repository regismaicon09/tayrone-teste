import { Component, OnInit, OnDestroy } from '@angular/core';
import { MaterializeDirective, toast } from 'angular2-materialize';
import { Subscription } from 'rxjs/Rx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../../login/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Usuario } from './../../usuario';
import { Links } from './../../links';

import { ClientesService } from './../clientes-service.service';
import { IFormCanDesactivate } from "../../guards/i-form-candesactivate";
import { Cliente } from '../cliente';
import { Strings } from '../../strings';
import { FileUploader } from 'ng2-file-upload';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-novo-lojista',
  templateUrl: './novo-lojista.component.html',
  styleUrls: ['./novo-lojista.component.css']
})

export class NovoLojistaComponent implements OnInit, IFormCanDesactivate {
  
  inscricao: Subscription;
  usuario: Usuario = new Usuario();  
  links: Links = new Links();

  formMudou: boolean = false;
  mostrarPreloader: boolean = false;
  mostrarPreloaderCL: boolean = false;
  clienteEncontrado: boolean = false;
  clienteJaCadastrado: boolean = false;

  cliente: Cliente = new Cliente();

  public maskcel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  // Acesso Modulos 
  dados_user: any;
  colaborador: any = {};
  id_colaborador: number;
  id: number;

  //ações
  acao: any;

  //permições
  mostrarPermissoes: boolean = false;

  //Requisiçoes
  cashModule: boolean = false; configCampanha: boolean = false; clientsModule: boolean = false; 
  configModule: boolean = false; powerbiModule: boolean = false; configRelatorio: boolean = false;
 
 //switchs
  check_cash: string; check_campanha: string; check_clientes: string; check_config: string; 
  check_powerbi: string; check_relatorios: string;

/////////////////////////

  //[(ngModel)]
  email: string = "";
  emailConfirm: string = "";
  celular: string = "";
  telefone: string = "";
  // cpfConsult: string;

  //Lista de empresas
  empresas: any[];
  company_id : Number;

  //switch
  mudou: boolean = false;


  // utilizado nas imagens 
  urlWebImage: string;
  uploaderWebImage: FileUploader;
 
  //titulo
  tilulo: string;

  constructor(private route: ActivatedRoute, private router: Router,private authService: AuthService, private http: HttpClient, private location: Location) { }

  ngOnInit() {
    
    $(function () { $('select').material_select(); });

    this.usuario = this.authService.getUsuario();

    this.buscarEmpresas();

    
    this.inscricao = this.route.queryParams.subscribe(
      (queryParams: any) => {
        this.id_colaborador = queryParams['u'];
        this.acao = queryParams['a'];
        
        //fazer requisicao para pegar os dados    
       this.setDados();    
      });
     
    //  this.authService.usuarioEmitter.subscribe(
    //    user => this.atualizaDados(user)
    //  );

  }

  ngOnDestroy() {
  }

  onInput() { //chamando quando o usuário digita algo em um dos campos
    this.formMudou = true;
  }


//BUSCA EMPRESAS CADASTRADAS DO BANCO

buscarEmpresas() {
  let dados: any = {};
 
  this.http.post(this.links.lbuscarDadosSelect, dados, {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  }).subscribe((data: any[]) => {
   //console.log(data);/////////////
    if (data.length == 0) { // houve um problema 
      toast(Strings.msgErro, 2000);
    } else {
      this.empresas = data;
      this.company_id = data["company_id"];
    }
  });
}

////////////////////


setDados() {
  console.log("entrou no setChecked()");///////////////////////////
  
  let dados: any = {};
  dados.acao = "select_user"
  dados.user_id = this.id_colaborador;
 
  this.mostrarPreloader = true;

  console.log("dados enviados >>:" + JSON.stringify(dados));
  this.http.post(this.links.leditLojista, dados, {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  }).subscribe((data) => {
    console.log(data);//////////////////////
    this.mostrarPreloader = false;
    if (data == 0) { // ocorreu um erro 
      toast(Strings.msgErro, 3000);
      //console.log(data.msg);//////////////////////
    } else {
      this.dados_user = data;

      this.colaborador.cashModule  = Boolean(this.dados_user.cashModule);
      this.colaborador.configCampanha  = Boolean(this.dados_user.configCampanha);
      this.colaborador.clientsModule  = Boolean(this.dados_user.clientsModule);
      this.colaborador.configModule  = Boolean(this.dados_user.configModule);
      this.colaborador.powerbiModule  = Boolean(this.dados_user.powerbiModule);
      this.colaborador.configRelatorio  = Boolean(this.dados_user.configRelatorio);


    }
  });

  //this.mostrarPreloader = false;
}


//chamado quando o usuario clica em algum checkbox
mudarFlagPermissao(tipo) {

  switch (tipo) {
    case "cash":
      this.cashModule = !this.cashModule;
      break;

    case "cam":
      this.configCampanha = !this.configCampanha;
      break;

    case "cli":
      this.clientsModule = !this.clientsModule;
      break;


    case "conf":
      this.configModule = !this.configModule;
      break;

    case "power":
      this.powerbiModule = !this.powerbiModule;
      break;

    case "rel":
      this.configRelatorio = !this.configRelatorio;
      break;
  }

}

mostrarEsconderPermissoes() {
  this.mostrarPermissoes = !this.mostrarPermissoes;
}

  
  zerardados() {
    this.formMudou = false; //para que se permita mudar de rota    
    this.email = "";
    this.emailConfirm = "";
    this.celular = "";
    this.telefone = "";
    this.clienteEncontrado = false;
    this.cliente = null;
  }

  onSubmit(form) { //chamado quando o usuário clica no botão salvar

   if (form['value']['email'] != form['value']['emailConfirm']) { //caso os campos sejam inválidos
     toast(Strings.msgEmailsDiferentes, 3000);
     } else {
      //console.log(this.cliente);   ///////  
      let dados: any = {};
     
      dados.acao = "novo";
      
     if (this.acao == 'update'){
      dados.acao = "update";
      dados.user_id = this.id_colaborador;
     } 

      dados.email = form['value']['email'];
      dados.nome = form['value']['nome'];
      dados.cargo = form['value']['cargo'];
      dados.data_nascimento = form['value']['data'];
      dados.celular = form['value']['celular'];
      dados.telefone = form['value']['telefone'];
      dados.company_id = form['value']['empresaSelecionada'];
      dados.cashModule = Number(this.cashModule);
      dados.configCampanha = Number(this.configCampanha);
      dados.clientsModule = Number(this.clientsModule);
      dados.configModule = Number(this.configModule);
      dados.powerbiModule = Number(this.powerbiModule);
      dados.configRelatorio = Number(this.configRelatorio);

    this.mostrarPreloader = true;

    console.log("dados enviados novo-Lojista>>:" + JSON.stringify(dados)); //////////////////////////
    this.http.post(this.links.lcadLojista, dados, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe(data => {
      console.log(data);//////////////////////
      this.mostrarPreloader = false;
      if (data == 0) { // ocorreu um erro 
        console.log("error aqui >>:" + data["msg"]);//////////////////////
        toast(Strings.msgErro, 3000);
      } else if (data["id"] == 2) {
        //console.log("error >>:" + data["msg"]);//////////////////////
        toast(Strings.msgClientExist, 3000);
        this.zerardados();
      }
      else {
        this.zerardados();
        toast(Strings.msgCadSuccess, 3000);
        //console.log("error >>:" + data["msg"]);//////////////////////
      }
    });
  }
  }

  desabilitaBotao(){
    
  }

  //caso o usuário altere alguma coisa no formulário, perguntar se ele realmente quer sair 
  //metodo da interface IFormCanDesactivate
  podeMudarRota() {
    if (this.formMudou) {
      if (confirm("Tem certeza que deseja sair dessa página?\nAs alterações serão perdidas.")) {
        return true;
      } else {
        return false;
      }
    }
    else {
      return true;
    }
  }

  cancelar() { //chamado quando o usuário clica no botão cancelar 
    //toast("Operação cancelada", 3000);
    this.formMudou = false;
    //this.router.navigate();
    this.location.back();
  }

}
