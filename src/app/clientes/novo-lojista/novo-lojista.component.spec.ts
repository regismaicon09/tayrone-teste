import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoLojistaComponent } from './novo-lojista.component';

describe('NovoLojistaComponent', () => {
  let component: NovoLojistaComponent;
  let fixture: ComponentFixture<NovoLojistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoLojistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoLojistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
