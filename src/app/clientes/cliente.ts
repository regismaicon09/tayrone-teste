export class Cliente{
    id: number;
    client_id: number;
    nome: string;
    cpf: string;
    total_cashback: number;
    total_retido: number;
    msg: string;
}