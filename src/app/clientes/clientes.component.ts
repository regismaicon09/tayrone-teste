import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from '@agm/core/services/google-maps-types';

import { Cliente } from "./cliente";
import { Links } from './../links';
import { Usuario } from './../usuario';

import { AuthService } from './../login/auth.service';
import { ClientesService } from './clientes-service.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})

export class ClientesComponent implements OnInit, OnDestroy {

  user: Usuario = new Usuario();
  links: Links = new Links();

  constructor(private router: Router, private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.getUsuario();
  }

  ngOnDestroy() {
  }

}

