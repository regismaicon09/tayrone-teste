import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClientesRoutingModule } from './clientes.routing.module';
import { MaterializeDirective, MaterializeModule } from 'angular2-materialize';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { TextMaskModule } from 'angular2-text-mask';
import { FocusModule } from 'angular2-focus';
import { ClientesService } from './clientes-service.service';

import { ClientesComponent } from './clientes.component';
import { NovoClienteComponent } from './novo-cliente/novo-cliente.component';
import { NovoLojistaComponent  } from'./novo-lojista/novo-lojista.component'
// import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';
// import { DetalheClienteComponent } from './detalhe-cliente/detalhe-cliente.component';


import { EditarDesactivateGuard } from "../guards/editar-desactivate.guards";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ClientesRoutingModule,
    HttpModule,
    HttpClientModule,
    TextMaskModule,
    FocusModule,
    MaterializeModule
  ],
  declarations: [
    ClientesComponent,
    // EditarClienteComponent,
    NovoClienteComponent,
    // DetalheClienteComponent,
    NovoLojistaComponent 
    
  ], exports: [

  ], providers: [
    ClientesService,
    EditarDesactivateGuard
  ]
})


export class ClientesModule { }
