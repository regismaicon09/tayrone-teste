import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Cliente } from './cliente';

@Injectable()
export class ClientesService {

  data: Cliente[];

  constructor(private http: HttpClient) { }

  getClientes(idLojista) {
    //console.log("chamou o getClientes() idLojista >>:"+idLojista); /////////////
    return this.http.get(`http://demo8339011.mockable.io/clientes`);
  }

  getCliente(id: number) {
    return this.http.get(`http://demo8339011.mockable.io/cliente`);
  }

  cadastrarCliente(cliente: any) {
    return 1;
  }

  editarCliente(cliente: any) {
    console.log("editar o cl >>:" + cliente.id);
    return 1;
  }

}
  // pesquisarCliente(nome: string) {
  //   let clientes;
  //   this.getClientes().subscribe(data => { clientes = data['clientes'] });
  //   for (let i = 0; i < clientes.length; i++) {
  //     if (clientes[i].nome == nome) {
  //       return clientes[i];
  //     }
  //   }
  //   return null;
  // }
