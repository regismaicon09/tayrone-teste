import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//componentes
import { ClientesComponent } from './clientes.component';
import { DetalheClienteComponent } from "./detalhe-cliente/detalhe-cliente.component";
import { EditarClienteComponent } from "./editar-cliente/editar-cliente.component";
import { NovoClienteComponent } from './novo-cliente/novo-cliente.component';
import { NovoLojistaComponent } from './novo-lojista/novo-lojista.component'; 

//guardas
import { AuthGuard } from "../guards/auth.guard";
import { EditarDesactivateGuard } from './../guards/editar-desactivate.guards';

const clientesRoutes: Routes = [
    { path: 'novo-cliente', component: NovoClienteComponent, canDeactivate:[EditarDesactivateGuard] },
    // {
    //     path: '', component: ClientesComponent, children: [
    //         { path: 'editar/:id', component: EditarClienteComponent, canDeactivate:[EditarDesactivateGuard] },
    //         { path: ':id', component: DetalheClienteComponent }            
    //     ]
    // },
    { path: 'novo-lojista', component: NovoLojistaComponent, canDeactivate:[EditarDesactivateGuard] },
];

@NgModule({
    imports: [
        RouterModule.forChild(clientesRoutes)
    ], exports:[
        RouterModule
    ]
})

export class ClientesRoutingModule { }