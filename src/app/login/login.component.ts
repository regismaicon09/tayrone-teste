import { Component, OnInit, HostBinding } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { Links } from './../links';

import { AuthService } from './auth.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  mostrarPreloader: boolean = false;
  mostrarSenha: boolean = false;

  //[(ngModel)]
  email: String; //para o usuario nao perder esses dados quando ele clica pra fazer o login e os dados estao incorretos
  senha: String;

  ipCliente: String;
  checkRememberMe: string;
  checkLoginAutomatico: string;
  flagRemeber: boolean = false;
  flagLogin: boolean = false;

  links: Links = new Links();

  constructor(private authService: AuthService, private router: Router, private http: HttpClient) {
  }

  ngOnInit() {

    this.authService.mostrarPreloaderLoginEmitter.subscribe(
      bol => this.mostrarPreloader = bol
    );

    //metodo utilizado para pegar o ip do cliente 
    this.pegarIpCliente();

    if (localStorage.getItem("loginAutomatico") != null) {
      if (localStorage.getItem("loginAutomatico") == "true") {
        this.checkLoginAutomatico = "checked";
        this.flagLogin = true;
      } else {
        this.checkLoginAutomatico = "";
      }
    }

    //console.log("lembrarUsuario >>:" + localStorage.getItem("lembrarUsuario") + " loginAutomatico >>:" + localStorage.getItem("loginAutomatico"));///////////////////

    if (localStorage.getItem("lembrarUsuario") != null) {
      if (localStorage.getItem("lembrarUsuario") == "true") {
        this.email = localStorage.getItem("PlataformaDataBoxEmail");
        this.senha = localStorage.getItem("PlataformaDataBoxSenha");
        this.checkRememberMe = "checked";
        this.flagRemeber = true;
      } else {
        this.email = "";
        this.senha = "";
        this.checkRememberMe = "";
      }
    }

    //metodo utilizado para fazer o Login automatico 
    if (localStorage.getItem("PlataformaDataBoxEmail") != null) {
      if (localStorage.getItem("PlataformaDataBoxEmail").length > 0 && localStorage.getItem("loginAutomatico") == "true") {
        this.mostrarPreloader = true;
        this.loginAutomatico();
      }
    }

  }

  pegarIpCliente() {
    this.http.get(this.links.lIP).subscribe(data => {
      //http://ipv4.myexternalip.com/json
      //console.log("dados da requisicao >:"+JSON.stringify(data));///////////////////////////
      this.ipCliente = data["query"];
    });
  }

  loginAutomatico() {
    this.http.get(this.links.lIP).subscribe(data => {
      this.ipCliente = data["query"];
      this.authService.fazerLoginLojista(localStorage.getItem("PlataformaDataBoxEmail"), localStorage.getItem("PlataformaDataBoxSenha"), this.ipCliente);
    });
  }

  mostrarEsconderSenha() {
    this.mostrarSenha = !this.mostrarSenha;
  }

  mudarRemember() {
    if (this.flagRemeber == false) {
      localStorage.setItem("lembrarUsuario", "true");
      this.flagRemeber = true;
    } else {
      localStorage.setItem("lembrarUsuario", "false");
      this.flagRemeber = false;
    }
  }

  mudarLoginAutomatico() {
    if (this.flagLogin == false) {
      localStorage.setItem("loginAutomatico", "true");
      this.flagLogin = true;
    } else {
      localStorage.setItem("loginAutomatico", "false");
      this.flagLogin = false;
    }
  }

  onSubmit(form) {
    let email = form['value']['email'];
    let senha = form['value']['senha'];

    this.mostrarPreloader = true;
    this.authService.fazerLoginLojista(email, senha, this.ipCliente);
  }

}
