import { Strings } from './../strings';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toast } from 'angular2-materialize';

import { Usuario } from '../usuario';
import { Links } from '../links';

@Injectable()
export class AuthService {

  //utilizado pelos guardas de rotas 
  lojistaAutenticado: boolean = false;
  // permissaoModuloCash: boolean = false;
  // permissaoModuloClientes: boolean = false;
  // permissaoModuloPowerbi: boolean = false;
  // permissaoModuloConfiguracoes: boolean = false;

  mostraMenuLojistaEmitter = new EventEmitter<boolean>();  //quando o usuário tenta fazer um login é emitido um evento, que eh escutado no app.component
  mostraMenuLateralEmitter = new EventEmitter<boolean>(); //utilizado pelo guarda 
  mostrarPreloaderLoginEmitter = new EventEmitter<boolean>(); // para esconder o preloader (login.component)
  usuarioEmitter = new EventEmitter<Usuario>();

  usuario: Usuario = new Usuario();
  links: Links = new Links();

  constructor(private router: Router, private http: HttpClient) {//router para fazer a navegação 
  }

  fazerLoginLojista(email, senha, ip) {

    let login: any = {};
    login.user = email;
    login.senha = senha;
    login.ip = ip;

    //console.log("dados enviados >>:" + JSON.stringify(login));/////////////////////////////////////////

    //precisa desse headrs por conta do servidor sestar em PHP 
    this.http.post(this.links.lAutenticacao, login, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe((data: Usuario) => {
      //console.log(data);//////////////////////
      if (data.idLojista == 0) { // usuario não foi encontrado, ou senha inválida
        this.lojistaAutenticado = false;
        toast(Strings.msgErrorLogin, 3000);
        this.mostrarPreloaderLoginEmitter.emit(false);
      } else {
        this.lojistaAutenticado = true;
        this.usuario = data;
        /* só para testes    */
        // this.usuario.cashModule = true;
        // this.usuario.clientesModule = true;
        // this.usuario.campanhaModule = true;
        // this.usuario.configuracoesModule = true;
        // this.usuario.powerbiModule = true;
        // this.usuario.relatoriosModule = true;
                                                                                      
        this.mostraMenuLojistaEmitter.emit(true);
        this.mostraMenuLateralEmitter.emit(true);
        this.usuarioEmitter.emit(data);

        localStorage.setItem("PlataformaDataBoxEmail", email);
        localStorage.setItem("PlataformaDataBoxSenha", senha);
        this.router.navigate(['/home']); //mudando a rota
      }
    });
  }

  atualizarDadosUsuario() {
    let login = { "user": localStorage.getItem("PlataformaDataBoxEmail"), "senha": localStorage.getItem("PlataformaDataBoxSenha") };
    this.http.post(this.links.lAutenticacao, login, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe((data: Usuario) => {
      // recebendo a requisição 
      console.log(data);
      if (data.idLojista == 0) { // usuario não foi encontrado, ou senha inválida
        toast(Strings.msgErro, 2000);
      } else {
        this.usuario = data;
        this.usuarioEmitter.emit(data);
      }
    });
  }

  fazerLogoutLojista() {
    //console.log("fez logout()"); ///////////////
    this.lojistaAutenticado = false;
    this.mostraMenuLojistaEmitter.emit(false);
    localStorage.setItem("loginAutomatico", "false");
    //localStorage.setItem("PlataformaDataBoxEmail", "");
    //localStorage.setItem("PlataformaDataBoxSenha", "");
    this.router.navigate(['']); //mudando a rota
  }

  esconderMenuLateral() {
    this.mostraMenuLateralEmitter.emit(false);
  }

  getUsuario() {
    return this.usuario;
  }

  getLojistaAutenticado() { // eh utilizado pelo guarda de rota que verifica se o usuario esta logado 
    return this.lojistaAutenticado;
  }

  getPermissaoModuloCash() { // eh utilizado pelo guarda de rota que verifica se o usuario esta logado 
    //return this.permissaoModuloCash;
    return this.usuario.cashModule;
  }

  getPermissaoModuloClientes() { // eh utilizado pelo guarda de rota que verifica se o usuario esta logado 
    //return this.permissaoModuloClientes;
    return this.usuario.clientesModule;
  }

  getPermissaoModuloConfiguracoes() {
    //return this.permissaoModuloConfiguracoes;
    return this.usuario.configuracoesModule;
  }

  getPermissaoModuloRelatorios() {
    //return this.permissaoModuloConfiguracoes;
    return this.usuario.relatoriosModule;
  }
  
}
