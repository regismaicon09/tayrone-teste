
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; //para usar o ngModel

import { AppRoutingModule } from './app.routing.module';
import { MaterializeModule } from 'angular2-materialize';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { FileUploadModule } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
import { EsquiciSenhaComponent } from './esquici-senha/esquici-senha.component';
import { MinhaContaComponent } from './minha-conta/minha-conta.component';
import { HomeComponent } from './home/home.component';

import { AuthService } from './login/auth.service'; //para a utilização do serviço de autenticação do usuário

import { AuthGuard } from './guards/auth.guard';

import { ClientesModuleGuard } from './guards/clientes-module.guard';
import { ConfiguracoesModuleGuard } from './guards/configuracoes-module.guard';
import { RelatoriosModuleGuard } from './guards/relatorios-module.guard';



@NgModule({
  declarations: [ //listamos aqui todos os componentes, diretivas e pipes
    AppComponent,
    LoginComponent,
    PaginaNaoEncontradaComponent,
    EsquiciSenhaComponent,
    MinhaContaComponent,
    HomeComponent
  ],
  imports: [//listamos aqui outros modulos
    BrowserModule,
    MaterializeModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD4a1FGfON60po6XwOyEyaIA0-gExmY0Q0'
    }),
    FileUploadModule
  ],
  providers: [ //declaramos os servicos que vao ficar diponiveis nesse modulo 
    AuthService,
    AuthGuard,
    ClientesModuleGuard,
    ConfiguracoesModuleGuard,
    RelatoriosModuleGuard
    
  ], exports: [

  ],
  bootstrap: [AppComponent] //componente que deve ser instanciado quando a aplicacao for "ligada"
})

export class AppModule { }
