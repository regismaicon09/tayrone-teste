import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import { toast } from 'angular2-materialize';

import { AuthService } from './../login/auth.service';
import { Strings } from '../strings';

@Injectable()
export class CashModuleGuard implements CanActivate, CanLoad {

    constructor(private authService: AuthService, private router: Router) { }

    private verificarAcessoLojista() {
        if (this.authService.getPermissaoModuloCash()) {
            //console.log("verifcou a rota e estava true");
            return true;
        }
        //caso contrário
        toast(Strings.msgErrorNoPermission, 2000);
        return false;
    }

    // aqui vai a lógica para ver se pode entrar em uma rota ou não 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        return this.verificarAcessoLojista();
    }

    canLoad(
        route: Route
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.verificarAcessoLojista();
    }

}