import { RouterStateSnapshot, ActivatedRouteSnapshot, CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx";
import { IFormCanDesactivate } from "./i-form-candesactivate";

@Injectable()
export class EditarDesactivateGuard implements CanDeactivate<IFormCanDesactivate> {
        
    constructor() {}
        canDeactivate(
            component: IFormCanDesactivate,
            route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot
        ): Observable<boolean>|Promise<boolean>|boolean {
            //console.log("chamou o guarda de desativação");/////////////////
            //no metodo podeMudarRota() vai a lógica para dar ou não permissão para sair da rota
            return component.podeMudarRota ? component.podeMudarRota():true;
    }
}