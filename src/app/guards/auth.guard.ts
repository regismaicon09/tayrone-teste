import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from "@angular/router";
import { Observable } from 'rxjs/Rx';

import { AuthService } from './../login/auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private authService: AuthService, private router: Router) { }

  private verificarAcessoLojista() {
    if (this.authService.getLojistaAutenticado()) {
      //console.log("verifcou a rota e estava true");
      return true;
    }
    //caso contrário
    this.router.navigate(['login']);
    return false;
  }

  // aqui vai a lógica para ver se pode entrar em uma rota ou não 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this.verificarAcessoLojista();
  }

  canLoad(
    route: Route
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.verificarAcessoLojista();
  }

}
