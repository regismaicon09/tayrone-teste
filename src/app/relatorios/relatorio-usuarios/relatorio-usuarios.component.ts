import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { toast } from 'angular2-materialize';

import { Links } from './../../links';
import { Usuario } from './../../usuario';

import { AuthService } from '../../login/auth.service';
import { Strings } from '../../strings';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-usuarios-relatorio',
  templateUrl: './relatorio-usuarios.component.html',
  styleUrls: ['./relatorio-usuarios.component.css']
})
export class UsuariosRelatorioComponent implements OnInit {

  usuario: Usuario = new Usuario();
  links: Links = new Links();
  dadosTabela: any;

  mostrarFiltro: boolean = false;
  mostrarPreloader: boolean = false;

  ordData: boolean = null;
  ordNome: boolean = null;
  ordEmail: boolean = null;

  //Lista de empresas
  empresas: any[];
  company_id : Number;

  /// checkbox
  id: boolean = false;
  id_user: boolean = false;

  msgOrdDados: string = Strings.msgOrdDados;

 

  constructor(private router: Router, private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {

    $(function () { $('select').material_select(); });

    this.usuario = this.authService.getUsuario();

    this.buscarEmpresas();

    //this.preencherTabela(form);

  }

  buscarEmpresas() {
    
    let dados: any = {};
   
    this.http.post(this.links.lbuscarDadosSelect, dados, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe((data: any[]) => {
     console.log(data);/////////////
      if (data.length == 0) { // houve um problema 
        toast(Strings.msgErro, 2000);
      } else {
        this.empresas = data;
        this.company_id = data["company_id"];
      }
    });
  }

  preencherTabela(form) {
    let dados: any = {};

    dados.id_company = form['value']['empresaSelecionada'];

    this.mostrarPreloader = true;
    console.log("dados enviados >>:" + JSON.stringify(dados)); //////////////////////////
    this.http.post(this.links.lRelUsuarios, dados, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe((data: any[]) => {
      console.log("servidor >>:" + JSON.stringify(data));//////////////////////
      this.mostrarPreloader = false;
      if (data.length == 0) { // ocorreu um erro 
        this.zerardados();
        toast(Strings.msgSemUsuarios, 3000);
      } else {
        this.dadosTabela = data;
      }
    });
  }

  ordenarData() {
    if (this.ordData == null || this.ordData == true) {
      this.ordData = false;
    } else {
      this.ordData = true;
    }

    this.ordNome = this.ordEmail = null;

    let sorrtedArray;
    if (this.ordData == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {

        let vec = String(n1.data).split("/");
        let d1 = Number(vec[0]);
        let m1 = Number(vec[1]);
        let a1 = Number(vec[2]);

        vec = String(n2.data).split("/");
        let d2 = Number(vec[0]);
        let m2 = Number(vec[1]);
        let a2 = Number(vec[2]);

        if (a1 > a2) {
          return 1;
        } else if ((a1 == a2) && (m1 > m2)) {
          return 1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 > d2)) {
          return 1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 == d2)) {
          return 0;
        } else {
          return -1;
        }

      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {

        let vec = String(n1.data).split("/");
        let d1 = Number(vec[0]);
        let m1 = Number(vec[1]);
        let a1 = Number(vec[2]);

        vec = String(n2.data).split("/");
        let d2 = Number(vec[0]);
        let m2 = Number(vec[1]);
        let a2 = Number(vec[2]);

        if (a1 > a2) {
          return -1;
        } else if ((a1 == a2) && (m1 > m2)) {
          return -1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 > d2)) {
          return -1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 == d2)) {
          return 0;
        } else {
          return 1;
        }

      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarNome() {
    if (this.ordNome == null || this.ordNome == true) {
      this.ordNome = false;
    } else {
      this.ordNome = true;
    }

    this.ordData = this.ordEmail = null;

    let sorrtedArray;
    if (this.ordNome == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.nome) > String(n2.nome)) {
          return 1;
        }
        if (String(n1.nome) < String(n2.nome)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.nome) < String(n2.nome)) {
          return 1;
        }
        if (String(n1.nome) > String(n2.nome)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarEmail() {
    if (this.ordEmail == null || this.ordEmail == true) {
      this.ordEmail = false;
    } else {
      this.ordEmail = true;
    }

    this.ordData = this.ordNome = null;

    let sorrtedArray;
    if (this.ordEmail == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.email) > String(n2.email)) {
          return 1;
        }
        if (String(n1.email) < String(n2.email)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.email) < String(n2.email)) {
          return 1;
        }
        if (String(n1.email) > String(n2.email)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  mostrarEsconderFiltro() {
    this.mostrarFiltro = !this.mostrarFiltro;
  }

  zerardados() {
    this.dadosTabela = false;
  }

  //metodo chamado quando o usuario mexe nos filtros 
  onSubmit(form) {
    if (form['value']['id'] == "") {
      this.id = false;
    } else {
      this.id = true;
    }

    if (form['value']['id_user'] == "") {
      this.id_user = false;
    } else {
      this.id_user = true;
    }

    this.mostrarEsconderFiltro();
    //console.log("genero >>:" + this.genero + " contato >:" + this.contato);//////
  }

}
