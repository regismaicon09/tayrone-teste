import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosRelatorioComponent } from './relatorio-usuarios.component';

describe('UsuariosRelatorioComponent', () => {
  let component: UsuariosRelatorioComponent;
  let fixture: ComponentFixture<UsuariosRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuariosRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
