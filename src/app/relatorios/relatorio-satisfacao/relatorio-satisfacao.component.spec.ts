import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatorioSatisfacaoComponent } from './relatorio-satisfacao.component';

describe('RelatorioSatisfacaoComponent', () => {
  let component: RelatorioSatisfacaoComponent;
  let fixture: ComponentFixture<RelatorioSatisfacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatorioSatisfacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatorioSatisfacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
