import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { toast } from 'angular2-materialize';

import { Links } from './../../links';
import { Usuario } from './../../usuario';
import { Strings } from './../../strings';

import { AuthService } from './../../login/auth.service';

@Component({
  selector: 'app-relatorio-satisfacao',
  templateUrl: './relatorio-satisfacao.component.html',
  styleUrls: ['./relatorio-satisfacao.component.css']
})
export class RelatorioSatisfacaoComponent implements OnInit {

  user: Usuario = new Usuario();
  links: Links = new Links();

  dadosGrafico: any = {};

  mostrarGrafico: boolean = false;
  mostrarPreloader: boolean = false;

  /** grafico      ********************************************************/

  /*  configuraçõs      */
  lineChartOptions: any = {
    responsive: true,
    scaleShowValues: true,
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          //return "R$" + Number(tooltipItem.yLabel);
          return "R$" + Number(tooltipItem.yLabel).toFixed(2).replace(/./g, function (c, i, a) {
            //console.log("c >>:"+c+ " i >>:"+i+" a >>:"+a)
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
          });
        }
      }
    }
  };
  lineChartLegend: boolean = true;
  lineChartType: string = 'line';
  lineChartColors: Array<any> = [
    {
      backgroundColor: 'transparent',
      borderColor: '#1565C0',
      pointBackgroundColor: '#0D47A1',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#009688',
      pointHoverBorderColor: '#009688'
    },
    { //se tiver outra linha
    }
  ];
  /* dados */
  public lineChartData: Array<any> = new Array();
  /******************************************************************************************************** */

  constructor(private authService: AuthService, private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.user = this.authService.getUsuario();
   //this.preencherGrafico();
  }

  ngOnDestroy() {
  }

  // preencherGrafico() {
  //   let dados: any = {};
  //   dados.company_id = this.user.idLojista;
  //   this.mostrarPreloader = true;

  //   //preencher o grafico 
  //   // this.http.post(this.links.lGraficoCashBack, dados, {
  //   //   headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  //   // }).subscribe(data => {
  //   //   //console.log(data);//////////////////////
  //   //   this.mostrarPreloader = false;
  //   //   if (data["id"] == 0) { // ocorreu um erro 
  //   //     toast(Strings.msgErro, 3000);
  //   //     //console.error("Error ao buscar dados para grafico");
  //   //   } else {
  //   //     this.dadosGrafico = data;
  //   //     this.lineChartData[0] = { data: this.dadosGrafico.valor, label: 'Cashback Concedido' };
  //   //     for (let i = 0; i < this.dadosGrafico.mes.length; i++) {
  //   //       //console.log(this.dadosGrafico.mes[i]);
  //   //       switch (this.dadosGrafico.mes[i]) {
  //   //         case "January":
  //   //           this.dadosGrafico.mes[i] = "Janeiro";
  //   //           break;
  //   //         case "February":
  //   //           this.dadosGrafico.mes[i] = "Fevereiro";
  //   //           break;
  //   //         case "March":
  //   //           this.dadosGrafico.mes[i] = "Março";
  //   //           break;
  //   //         case "April":
  //   //           this.dadosGrafico.mes[i] = "Abril";
  //   //           break;
  //   //         case "May":
  //   //           this.dadosGrafico.mes[i] = "Maio";
  //   //           break;
  //   //         case "June":
  //   //           this.dadosGrafico.mes[i] = "Junho";
  //   //           break;
  //   //         case "July":
  //   //           this.dadosGrafico.mes[i] = "Julho";
  //   //           break;
  //   //         case "August":
  //   //           this.dadosGrafico.mes[i] = "Agosto";
  //   //           break;
  //   //         case "September":
  //   //           this.dadosGrafico.mes[i] = "Setembro";
  //   //           break;
  //   //         case "October":
  //   //           this.dadosGrafico.mes[i] = "Outubro";
  //   //           break;
  //   //         case "November":
  //   //           this.dadosGrafico.mes[i] = "Novembro";
  //   //           break;
  //   //         case "December":
  //   //           this.dadosGrafico.mes[i] = "Dezembro";
  //   //           break;
  //   //       }
  //   //     }
  //   //     this.mostrarGrafico = true;
  //   //   }
  //   // });
  // }
}
