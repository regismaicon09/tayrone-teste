import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { google } from '@agm/core/services/google-maps-types';
import { toast } from 'angular2-materialize';
//import { trigger, state, style, animate, transition, keyframes } from '@angular/animations'

import { Links } from './../../links';
import { Usuario } from './../../usuario';
import { Strings } from './../../strings';

import { AuthService } from './../../login/auth.service';
import { from } from 'rxjs/observable/from';

@Component({
  selector: 'app-relatorio-clientes',
  templateUrl: './relatorio-clientes.component.html',
  styleUrls: ['./relatorio-clientes.component.css'],
  // animations: [
  //   trigger('focusPanel', [
  //     // What happens when toggleState is true
  //     state('true', style({ maxHeight: '200px' })),
  //     // What happens when toggleState is false
  //     state('false', style({ maxHeight: 0, padding: 0, display: 'none' })),
  //     transition('false => true', animate('500ms')),
  //     transition('true => false', animate('500ms'))
  //   ]),

  //   trigger('buttonPanel', [
  //     // What happens when toggleState is true
  //     state('true', style({ maxHeight: '200px' })),
  //     // What happens when toggleState is false
  //     state('false', style({ maxHeight: 0, padding: 0, display: 'none' })),
  //     transition('false => true', animate('5s')),
  //     transition('true => false', animate('500ms'))
  //   ]),

  // ]
})
export class RelatorioClientesComponent implements OnInit {

  teste: any[] = [{ lat: -18.917393, lgn: -48.238261, title: "T1", desc: "teste 1" }, { lat: -18.922965, lgn: -48.253045, title: "T2", desc: "teste 2" }, { lat: -18.918577, lgn: -48.258781, title: "T3", desc: "teste 3" }];

  mapChartData = {
    chartType: 'GeoChart',
    dataTable: [
      ['City', 'Teste'],
      ['Uberlandia, MG', 200],
      ['Montes Claros, MG', 80]
      // ['BR-MT', 1944],
      // ['BR-PI', 799],
      // ['BR-AC', 248],
      // ['BR-PB', 832],
      // ['BR-PA', 282],
      // ['BR-RJ', 421],
      // ['BR-MG', 309],
      // ['BR-ES', 268],
      // ['BR-TO', 471],
      // ['BR-RR', 740],
      // ['BR-DF', 643],
      // ['BR-AM', 239],
      // ['BR-RO', 138],
      // ['BR-GO', 347],
      // ['BR-RN', 649],
      // ['BR-SE', 176],
      // ['BR-PE', 151],
      // ['BR-AP', 625],
      // ['BR-SC', 171],
      // ['BR-RS', 435],
      // ['BR-AL', 277],
      // ['BR-MS', 253],
      // ['BR-MA', 144],
      // ['BR-CE', 743],
      // ['BR-BA', 585],
      // ['BR-PR', 215],
      // ['BR-SP', 777]
    ],
    options: {
      region: 'BR',
      displayMode: 'markers',
      resolution: 'provinces',
      backgroundColor: 'transparent',
      datalessRegionColor: 'transparent',
      defaultColor: 'transparent'
    }
  };

  // colorAxis: { dentro de options 
  //   colors: ['#acb2b9', '#2f3f4f']
  // } 

  // ['City', 'Views'],
  // ['Recife, PE', 200],
  // ['Manaus, AM', 300],
  // ['Santos, SP', 400],
  // ['Campinas, SP', 400],

  /////////////////////////////////////

  /* chart de barras */
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      yAxes: [{ id: 'y-axis-1', type: 'linear', position: 'left', ticks: { min: 0, max: 100 } }]
    }
  };

  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartColors: Array<any> = [
    {
      backgroundColor: '#2196F3',
      borderColor: '#1565C0',
      pointBackgroundColor: '#0D47A1',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#F44336',
      pointHoverBorderColor: '#F44336'
    },
    { //se tiver outra linha
    }
  ];

  public barChartLabelsIdade: string[] = ['-18', '19-30', '30-50', '+50'];

  public barChartDataIdade: any[] = [
    { data: [10, 30, 20, 10] }
  ];

  public barChartLabelsConheceu: string[] = ['Face', 'SITC', 'Ind', 'TV'];

  public barChartDataConheceu: any[] = [
    { data: [65, 59, 80, 81] }
    // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  /*       ///////////////////////////            */

  /* piechart de barras */
  public pieChartOptions: any = {
    responsive: true
  };

  public pieChartLabels: string[] = ['F 60%', 'M 40%'];
  public pieChartData: number[] = [600, 400];
  public pieChartType: string = 'pie';

  /*       ///////////////////////////            */

  user: Usuario = new Usuario();
  links: Links = new Links();

  telaPequena: boolean = false;
  telaGrande: boolean = true;

  dadosTabela: any;
  mostrarFiltro: boolean = false;
  mostrarPreloader: boolean = false;

  ordData: boolean = null;
  ordNome: boolean = null;
  ordCpf: boolean = null;
  ordEmail: boolean = null;
  ordContato: boolean = null;
  ordGenero: boolean = null;

  /// checkbox
  genero: boolean = false;
  contato: boolean = false;

  lat: number = 51.678418;
  lng: number = 7.809007;

  msgOrdDados: string = Strings.msgOrdDados;

  state: string = 'false';
  stateb: string = 'false';

  constructor(private router: Router, private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.getUsuario();
    this.getUserLocation();
    this.preencherTabela();
    this.setTamanhoTela(window.innerWidth);
  }

  //para pegar a atual localização do usuário 
  getUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          //console.log("lat >>:"+this.lat + " long >>:"+this.lng); ///////////////////////////////////
        }
      );
    }
  }

  ngOnDestroy() {
  }

  setTamanhoTela(value) {//metodo chamado quando a tela muda de tamanho
    //console.log("tamanho da tela >>:" + value);/////////////////////
    if (value <= 992) { //caiu no .m ou .s do grid
      this.telaGrande = false;
      this.telaPequena = true;
    } else {
      this.telaGrande = true;
      this.telaPequena = false;
    }
  }

  preencherTabela() {
    let dados: any = {};
    dados.id_company = this.user.idLojista;
    dados.acao = "";
    this.mostrarPreloader = true;
    //console.log("dados enviados >>:" + JSON.stringify(dados)); //////////////////////////
    this.http.post(this.links.lClientesCadastrados, dados, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe((data: any[]) => {
      //console.log(data);//////////////////////
      this.mostrarPreloader = false;
      if (data.length == 0) { // ocorreu um erro 
        toast(Strings.msgSemClientes, 3000);
      } else {
        this.dadosTabela = data;
      }
    });
  }

  ordenarData() {
    if (this.ordData == null || this.ordData == true) {
      this.ordData = false;
    } else {
      this.ordData = true;
    }

    this.ordNome = this.ordCpf = this.ordEmail = this.ordContato = this.ordGenero = null;

    let sorrtedArray;
    if (this.ordData == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {

        let vec = String(n1.data).split("/");
        let d1 = Number(vec[0]);
        let m1 = Number(vec[1]);
        let a1 = Number(vec[2]);

        vec = String(n2.data).split("/");
        let d2 = Number(vec[0]);
        let m2 = Number(vec[1]);
        let a2 = Number(vec[2]);

        if (a1 > a2) {
          return 1;
        } else if ((a1 == a2) && (m1 > m2)) {
          return 1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 > d2)) {
          return 1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 == d2)) {
          return 0;
        } else {
          return -1;
        }

      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {

        let vec = String(n1.data).split("/");
        let d1 = Number(vec[0]);
        let m1 = Number(vec[1]);
        let a1 = Number(vec[2]);

        vec = String(n2.data).split("/");
        let d2 = Number(vec[0]);
        let m2 = Number(vec[1]);
        let a2 = Number(vec[2]);

        if (a1 > a2) {
          return -1;
        } else if ((a1 == a2) && (m1 > m2)) {
          return -1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 > d2)) {
          return -1;
        } else if ((a1 == a2) && (m1 == m2) && (d1 == d2)) {
          return 0;
        } else {
          return 1;
        }

      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarNome() {
    if (this.ordNome == null || this.ordNome == true) {
      this.ordNome = false;
    } else {
      this.ordNome = true;
    }

    this.ordData = this.ordCpf = this.ordEmail = this.ordContato = this.ordGenero = null;

    let sorrtedArray;
    if (this.ordNome == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.nome) > String(n2.nome)) {
          return 1;
        }
        if (String(n1.nome) < String(n2.nome)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.nome) < String(n2.nome)) {
          return 1;
        }
        if (String(n1.nome) > String(n2.nome)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarCPF() {
    if (this.ordCpf == null || this.ordCpf == true) {
      this.ordCpf = false;
    } else {
      this.ordCpf = true;
    }

    this.ordData = this.ordNome = this.ordEmail = this.ordContato = this.ordGenero = null;

    let sorrtedArray;
    if (this.ordCpf == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.cpf) > String(n2.cpf)) {
          return 1;
        }
        if (String(n1.cpf) < String(n2.cpf)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.cpf) < String(n2.cpf)) {
          return 1;
        }
        if (String(n1.cpf) > String(n2.cpf)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarEmail() {
    if (this.ordEmail == null || this.ordEmail == true) {
      this.ordEmail = false;
    } else {
      this.ordEmail = true;
    }

    this.ordData = this.ordNome = this.ordCpf = this.ordContato = this.ordGenero = null;

    let sorrtedArray;
    if (this.ordEmail == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.email) > String(n2.email)) {
          return 1;
        }
        if (String(n1.email) < String(n2.email)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.email) < String(n2.email)) {
          return 1;
        }
        if (String(n1.email) > String(n2.email)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarContato() {
    if (this.ordContato == null || this.ordContato == true) {
      this.ordContato = false;
    } else {
      this.ordContato = true;
    }

    this.ordData = this.ordNome = this.ordCpf = this.ordEmail = this.ordGenero = null;

    let sorrtedArray;
    if (this.ordContato == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.contato) > String(n2.contato)) {
          return 1;
        }
        if (String(n1.contato) < String(n2.contato)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.contato) < String(n2.contato)) {
          return 1;
        }
        if (String(n1.contato) > String(n2.contato)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  ordenarGenero() {
    if (this.ordGenero == null || this.ordGenero == true) {
      this.ordGenero = false;
    } else {
      this.ordGenero = true;
    }

    this.ordData = this.ordNome = this.ordCpf = this.ordContato = this.ordEmail = null;

    let sorrtedArray;
    if (this.ordGenero == false) {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.genero) > String(n2.genero)) {
          return 1;
        }
        if (String(n1.genero) < String(n2.genero)) {
          return -1;
        }
        return 0;
      });
    } else {
      sorrtedArray = this.dadosTabela.sort((n1, n2) => {
        if (String(n1.genero) < String(n2.genero)) {
          return 1;
        }
        if (String(n1.genero) > String(n2.genero)) {
          return -1;
        }
        return 0;
      });
    }

    this.dadosTabela = sorrtedArray;
  }

  //metodo chamado quando o usuario mexe nos filtros 
  onSubmit(form) {
    if (form['value']['genero'] == "") {
      this.genero = false;
    } else {
      this.genero = true;
    }

    if (form['value']['contato'] == "") {
      this.contato = false;
    } else {
      this.contato = true;
    }

    this.mostrarEsconderFiltro();

    //console.log("genero >>:" + this.genero + " contato >:" + this.contato);//////
  }

  mostrarEsconderFiltro() {
    this.mostrarFiltro = (this.mostrarFiltro == false ? true: false);
    this.state = (this.state === 'false' ? 'true' : 'false');
    this.stateb = (this.stateb === 'false' ? 'true' : 'false');
  }

  addCliente() {
    this.router.navigate(['/clientes/novo-cliente']);
  }

}
