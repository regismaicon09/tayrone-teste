import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RelatoriosComponent } from './relatorios.component';
import { RelatorioSatisfacaoComponent } from './relatorio-satisfacao/relatorio-satisfacao.component';
import { RelatorioClientesComponent } from './relatorio-clientes/relatorio-clientes.component';
import { UsuariosRelatorioComponent } from './relatorio-usuarios/relatorio-usuarios.component';

import { AuthGuard } from "../guards/auth.guard";

const cashbackRoutes: Routes = [

    { path: '', component: RelatoriosComponent },
    
    { path: 'relatorio-satisfacao', component: RelatorioSatisfacaoComponent },
    { path: 'relatorio-clientes', component: RelatorioClientesComponent },
    { path: 'relatorio-usuarios', component: UsuariosRelatorioComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(cashbackRoutes)
    ], exports:[
        RouterModule
    ]
})

export class RelatoriosRoutingModule { }