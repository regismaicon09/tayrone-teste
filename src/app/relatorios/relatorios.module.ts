import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';

import { RelatoriosRoutingModule } from './relatorios.routing.module';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { MyDatePickerModule } from 'mydatepicker';
import { FocusModule } from 'angular2-focus';
import { ChartsModule } from 'ng2-charts';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { TextMaskModule } from 'angular2-text-mask';
import { AgmCoreModule } from '@agm/core';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RelatoriosComponent } from './relatorios.component';
import { RelatorioSatisfacaoComponent } from './relatorio-satisfacao/relatorio-satisfacao.component';


import { RelatorioClientesComponent } from './relatorio-clientes/relatorio-clientes.component';
import { UsuariosRelatorioComponent } from './relatorio-usuarios/relatorio-usuarios.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RelatoriosRoutingModule,
    MaterializeModule,
    HttpModule,
    HttpClientModule,
    MyDatePickerModule,
    FocusModule,
    ChartsModule,
    Ng2GoogleChartsModule,
    TextMaskModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD4a1FGfON60po6XwOyEyaIA0-gExmY0Q0'
    })
  ],
  declarations: [
    RelatoriosComponent,
    RelatorioSatisfacaoComponent,
    RelatorioClientesComponent,
    UsuariosRelatorioComponent
    
  ], exports: [

  ], providers: [

  ]
})

export class RelatoriosModule { }
