import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { toast } from 'angular2-materialize';

import { Usuario } from './../usuario';
import { Links } from './../links';
import { Strings } from './../strings';

import { AuthService } from './../login/auth.service';

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.css']
})

export class RelatoriosComponent implements OnInit, OnDestroy {

  user: Usuario = new Usuario();
  links: Links = new Links();
  dadosGrafico: any ={};

  mostrarGrafico: boolean = false;
  mostrarPreloader: boolean = false;

  /** grafico      ********************************************************/

  /*  configuraçõs      */
  lineChartOptions: any = {
    responsive: true,
    scaleShowValues: true, 
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          //return "R$" + Number(tooltipItem.yLabel);
          return "R$" + Number(tooltipItem.yLabel).toFixed(2).replace(/./g, function (c, i, a) {
            //console.log("c >>:"+c+ " i >>:"+i+" a >>:"+a)
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
          });
        }
      }
    }
  };
  lineChartLegend: boolean = true;
  lineChartType: string = 'line';
  lineChartColors: Array<any> = [
    {
      backgroundColor: 'transparent',
      borderColor: '#1565C0',
      pointBackgroundColor: '#0D47A1',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#009688',
      pointHoverBorderColor: '#009688'
    },
    { //se tiver outra linha
    }
  ];
  /* dados */
  public lineChartData: Array<any> = new Array();
  /******************************************************************************************************** */
  /////////////////////////////////////////////

  constructor(private authService: AuthService, private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.user = this.authService.getUsuario();
    this.preencherGrafico();
  }

  ngOnDestroy() {
  }

  preencherGrafico() {
    let dados: any = {};
    dados.company_id = this.user.idLojista;
    this.mostrarPreloader = true;

    
  }

}
