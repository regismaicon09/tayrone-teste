export class Strings{

// geral  ***
    //nome do app
    public static nomeApp: string = "MicsBox";    
    //msg de erro (geralmente utilizada quando da erro ao buscar dados do servidor)
    public static msgErro: string = "Ops, algo deu errado. Tente novamente mais tarde.";
    //msg que aparece quando quando o usuario passa o mouse perto do nome de uma coluna de uma table
    public static msgOrdDados: string = "Clique para ordenar os dados";    

// modulo clientes
//novo- cliente 
    public static msgEmpresaCadastrad: String = "Empresa já cadastrada"; 



//modulo campanha
//nova-campanha
    //msg exibida quando o usuario passa o mouse perto do checkbox de intervalo 
    public static msgCheckIntervaloIdade: string = "Mostrar intervalo para idade"; 
    //msg exibida quando se passa o mouse sobre o botao circular com simbolo de X
    public static msgDTodos: string = "Desmarcar todos"; 
    //msg exibida quando se passa o mouse sobre o botao circular com simbolo de check
    public static msgMTodos: string = "Marcar todos";         


//modulo cashback ***
//cash-lancar
    //msg quando se tenta fazer um vinculo com um cliente que ja existe (tb utilizado em novo-cliente)
    public static msgClientExist: string = "Ops, esse cliente já está cadastrado.";  
    //msg de vinculo realizado com sucesso 
    public static msgCLSuccsess: string = "Vinculo realizado com sucesso!";     
    //msg quando se tenta fazer um vinculo com um cpf incorreto 
    public static msgErroCpf: string = "CPF inválido"; 
    //msg quando os emails digitados nao sao iguais (tb utilizado em novo-cliente)
    public static msgEmailsDiferentes: string = "Os emails não coincidem"; 
    //msg 
    public static msgFaltaCompletarCadastro: string = "O cliente deve primeiramente completar o seu cadastro";  
    //msg cash lncado com sucesso 
    public static msgValLSuccess: string = "Valor lançado com sucesso!"; 
    //msg mostrada quando o eh realizado o lancameto do cah + o cad do cliente (tb utilizado em novo-cliente)
    public static msgCadSuccess: string = "Cadastro realizado com sucesso!"; 

//cash-resgatar     
    //msg 
    public static msgVRSucess: string = "Valor retirado com sucesso!"; 
    //msg campos preenchidos de forma incorreta
    public static msgInvalidInputs: string = "Os campos devem ser preenchidos corretamente"; 

//modulo configuracoes ***
//configurar-notificacao
    //msg 
    public static msgNotConfSuccess: string = "Notificação configurada com sucesso!"; 
//configurar-cashback    
    //msg 
    public static msgValAtSuccess: string = "Alteração realizada com sucesso!"; 
    //msg 
    public static msgErrorInvalidVal: string = "Por favor preencha o valor corretamente" ; 
    
//esqueci-senha ***    
    //msg 
    public static msgErrorInvalidEmail: string = "Ops, este e-mail não foi encontrado :("; 
    //msg 
    public static msgSendEmailSuccess: string = "E-mail enviado com sucesso! Confira sua caixa de entrada.";  

//modules guard ***    
    //msg 
    public static msgErrorNoPermission: string = "Você não tem permissão para acessar essa funcionalidade :("; 

//login, authService ***
    //msg usuario o ou senha invalidos
    public static msgErrorLogin: string = "Por favor, verifique seu usuário ou senha. :("; 

//minha-conta  ***
    //msg 
    public static msgInvalidPassAtual: string = "Senha atual não confere."; 
    //msg 
    public static msgPasswordAltSuccess: string = "Senha alterada com sucesso!"; 

//modulo power-bi ***
//tr.component    
    //msg 
    public static msgSemDados: string = "Não há registros neste período"; 

//mudulo relatorios ***
//relatorio-usuarios     
    //msg 
    public static msgSemUsuarios: string = "Não foram encontrados usuários cadastrados."; 

//relatorio-clientes    
    //msg 
    public static msgSemClientes: string = "Não existem clientes cadastrados."; 

//relatorio-cash
    //msg 
    public static msgSemCashbackConcedido: string = "Não existe cashback concedido "; 
    //msg 
    public static msgErrorIntervaloDatas: string = "A data incial deve ser menor que a data final";  
    
    // //msg 
    // public static msg: string = ; 
    // //msg 
    // public static msg: string = ; 
    // //msg 
    // public static msg: string = ; 
    // //msg 
    // public static msg: string = ; 
    // //msg 
    // public static msg: string = ; 

}