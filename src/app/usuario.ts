export class Usuario {
   
    idLojista: number;
    idUser: number;
    nome: String;
    uploaderImage: string;
    linkImageUser: string;
    dir: String;    
    nameImageUser:String;
    urlVG: any;
    urlC3: any;
    urlMI: any;
   
    notaGatilho: number;
    emailGatilho: string;
    statusGatilho: number;
   
    campanhaModule: boolean; // falta vim a resposta
    cashModule: boolean;
    clientesModule: boolean;
    configuracoesModule: boolean;
    powerbiModule: boolean;
    relatoriosModule: boolean; // falta vim a resposta

}