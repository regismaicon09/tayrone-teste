export class Table {
    date: string;
    email: string;
    nota_ambiente: string;
    nota_atendimento: string;
    nota_preco: string;
    nota_qualidade: string;
    nota_tempo: string;
    nps: number;
    sugestao: string;
 }