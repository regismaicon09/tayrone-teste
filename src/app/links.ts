export class Links {
    

//diversos
    //veficar se o cliente esta cadastrado ou nao
    public lVerificaCPF: string = "http://regismaicon.com.br/Micsbox/verificaCPF.php";      

    
//clientes
   //Cadastro empresa

    // cadatrar novas empresas
    public lcadEmpresa: string = "http://regismaicon.com.br/Micsbox/cadastraEmpresa.php";   
    //cadastar novos lojistas
    public lcadLojista: string = "http://regismaicon.com.br/Micsbox/cadastraLojista.php";
    //para buscar os dados para preencher os selects 
    public lbuscarDadosSelect: string = "http://regismaicon.com.br/Micsbox/listaEmpresas.php"; 

    //Busca o id do usúario que for passado
    public leditLojista: string = "http://regismaicon.com.br/Micsbox/buscaUser.php";

//configuracoes
    //config cashback
    public lConfCash: string = "http://regismaicon.com.br/Micsbox/confCashback.php";
    //config notificacao
    public lConfNotificacao: string = "http://regismaicon.com.br/Micsbox/confGatilho.php";
    
//esquici-senha
    //enviar e-mail para o servidor (*)
    public lESenha: string = "http://regismaicon.com.br/Micsbox/sendEmailRecSenha.php";
        
//login
    //buscar IP
    public lIP: string="http://ip-api.com/json";
    //authservice
    public lAutenticacao:string = "http://regismaicon.com.br/Micsbox/verifica_login_web_user.php";

//relatorios
    
    //buscar clientes cadastrados 
    public lClientesCadastrados: string = "http://regismaicon.com.br/Micsbox/filterUserReport.php";  
    // relatorios de usuarios
    public lRelUsuarios: string = "http://regismaicon.com.br/Micsbox/filterLojistaUserReport.php"; 

//usuarios
    //atualizar senha (*)
    public lUpdateSenha: string = "http://regismaicon.com.br/Micsbox/update_password.php";
    
}