import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { toast } from 'angular2-materialize';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Usuario } from '../usuario';
import { Links } from './../links';

import { AuthService } from './../login/auth.service';
import { Strings } from '../strings';

@Component({
  selector: 'app-esquici-senha',
  templateUrl: './esquici-senha.component.html',
  styleUrls: ['./esquici-senha.component.css']
})
export class EsquiciSenhaComponent implements OnInit {

  usuario: Usuario = new Usuario();
  links: Links = new Links();
  mostrarPreloader: boolean = false;

  constructor(private router: Router, private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {
    this.usuario = this.authService.getUsuario();
  }

  onSubmit(form) {
    let email: string = form['value']['email'];
    this.mostrarPreloader = true;

    let recuperar = { "email": email };
    this.http.post(this.links.lESenha, recuperar, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe(data => {
      //console.log(data);//////////////////////
      if (data['id'] == 0) { // usuario não foi encontrado, ou senha inválida
        toast(Strings.msgErrorInvalidEmail, 2000);
        this.mostrarPreloader = false;
      } else {
        toast(Strings.msgSendEmailSuccess, 5000);
        this.mostrarPreloader = false;
        this.router.navigate(['']); //voltar para a home
      }
    });

  }

}
