import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsquiciSenhaComponent } from './esquici-senha.component';

describe('EsquiciSenhaComponent', () => {
  let component: EsquiciSenhaComponent;
  let fixture: ComponentFixture<EsquiciSenhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsquiciSenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsquiciSenhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
