import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Usuario } from '../../usuario';
import { Links } from './../../links';

import { AuthService } from '../../login/auth.service';
import { Strings } from '../../strings';

@Component({
  selector: 'app-configurar-notificacao',
  templateUrl: './configurar-notificacao.component.html',
  styleUrls: ['./configurar-notificacao.component.css']
})
export class ConfigurarNotificacaoComponent implements OnInit {

  user: Usuario = new Usuario();
  links: Links = new Links();
  
  emailGatilho: String;
  notaGatilho: number;
  statusGatilho: number;
  flagGatilho: boolean;
  
  mostrarPreloader: boolean = false;
  check: String;

  constructor(private router: Router, private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.getUsuario();
    this.setChecked();

    this.authService.usuarioEmitter.subscribe(
      user => this.atualizaDados(user)
    );
  }

  atualizaDados(u) {
    this.user = u;
    this.setChecked();
  }

  setChecked() {
    //console.log("entrou no setChecked()");///////////////////////////
    this.emailGatilho = this.user.emailGatilho;
    this.notaGatilho = this.user.notaGatilho;
    this.statusGatilho = this.user.statusGatilho;

    if (this.statusGatilho == 1) {
      this.check = "checked";
      this.flagGatilho = true;
    } else {
      this.check = "";
      this.flagGatilho = false;
    }

    this.mostrarPreloader = false;
  }

  mudarFlagGatilho(){
    this.flagGatilho = !this.flagGatilho;
  }

  onSubmit(form) {
    this.mostrarPreloader = true;

    if (this.flagGatilho) {
      this.statusGatilho = 1;
    } else {
      this.statusGatilho = 0;
    }

    let dados: any = {};
    dados.company_id = this.user.idLojista;
    dados.email_gatilho = form['value']['email'];
    dados.nota_gatilho = form['value']['nota'];
    dados.status_gatilho = this.statusGatilho;

    // let dados: any = {};
    // dados.id = this.user.idLojista;
    // dados.email = email;
    // dados.nota = nota;
    // dados.statusGatilho = this.statusGatilho;

    //console.log("dados enviados" + JSON.stringify(dados));//////////////////////

    this.http.post(this.links.lConfNotificacao, dados, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    }).subscribe(data => {
      //console.log(data);//////////////////////
      this.mostrarPreloader = false;
      if (data['id'] == 0) { // usuario não foi encontrado, ou senha inválida
        toast(Strings.msgErro, 2000);
      } else {
        toast(Strings.msgNotConfSuccess, 2000);
        this.mostrarPreloader = true;
        this.authService.atualizarDadosUsuario();
        //this.router.navigate(['/cashback']); //voltar para a home
      }
    });
  }
}
