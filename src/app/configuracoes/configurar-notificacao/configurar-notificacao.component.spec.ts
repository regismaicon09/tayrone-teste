import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarNotificacaoComponent } from './configurar-notificacao.component';

describe('ConfigurarNotificacaoComponent', () => {
  let component: ConfigurarNotificacaoComponent;
  let fixture: ComponentFixture<ConfigurarNotificacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurarNotificacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarNotificacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
