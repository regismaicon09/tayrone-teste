import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { Usuario } from '../../usuario';
import { AuthService } from '../../login/auth.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-configurar-permissao-usuario',
  templateUrl: './configurar-permissao-usuario.component.html',
  styleUrls: ['./configurar-permissao-usuario.component.css']
})
export class ConfigurarPermissaoUsuarioComponent implements OnInit {

  inscricao: Subscription;
  user: Usuario = new Usuario();

  mostrarPreloader: boolean = false;

  colaborador: any = {};

  id_colabordor: number;
  nome_colaborador: string;
  funcao_colaborador: string;


  flag_cash: boolean = false; flag_campanha: boolean = false; flag_clientes: boolean = false; flag_config: boolean = false; flag_powerbi: boolean = false; flag_relatorios: boolean = false;
  check_cash: string; check_campanha: string; check_clientes: string; check_config: string; check_powerbi: string; check_relatorios: string;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private authService: AuthService) { }

  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        this.id_colabordor = params['u'];
        //fazer requisicao para pegar os dados    
        //this.setDados();    
      });

    this.user = this.authService.getUsuario();

    // this.authService.usuarioEmitter.subscribe(
    //   user => this.atualizaDados(user)
    // );

  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

  atualizaDados(c) {
    this.colaborador = c;
    this.setDados();
  }

  //chamado para mostrar a atual configuracao dos modulos 
  setDados() {
    //console.log("entrou no setChecked()");///////////////////////////

    this.nome_colaborador = this.colaborador.nome;
    this.funcao_colaborador = this.colaborador.funcao;

    this.flag_cash = this.colaborador.flag_cash;
    this.flag_campanha = this.colaborador.flag_campanha;
    this.flag_clientes = this.colaborador.flag_clientes;
    this.flag_config = this.colaborador.flag_config;
    this.flag_powerbi = this.colaborador.flag_powerbi;
    this.flag_relatorios = this.colaborador.flag_relatorios;

    if (this.flag_cash) {
      this.check_cash = "checked";
    } else {
      this.check_cash = "";
    }

    if (this.flag_campanha) {
      this.check_campanha = "checked";
    } else {
      this.check_campanha = "";
    }

    if (this.flag_clientes) {
      this.check_clientes = "checked";
    } else {
      this.check_clientes = "";
    }

    if (this.flag_config) {
      this.check_config = "checked";
    } else {
      this.check_config = "";
    }

    if (this.flag_powerbi) {
      this.check_powerbi = "checked";
    } else {
      this.check_powerbi = "";
    }

    if (this.flag_relatorios) {
      this.check_relatorios = "checked";
    } else {
      this.check_relatorios = "";
    }

    //this.mostrarPreloader = false;
  }

  //chamado quando o usuario clica em algum checkbox
  mudarFlagPermissao(tipo) {

    switch (tipo) {
      case "cash":
        this.flag_cash = !this.flag_cash;
        break;

      case "cam":
        this.flag_campanha = !this.flag_campanha;
        break;

      case "cli":
        this.flag_clientes = !this.flag_clientes;
        break;


      case "conf":
        this.flag_config = !this.flag_config;
        break;

      case "power":
        this.flag_powerbi = !this.flag_powerbi;
        break;

      case "rel":
        this.flag_relatorios = !this.flag_relatorios;
        break;
    }

  }

  onSubmit(f) {
    let dados: any = {};
    dados.nome = f['value']['nome'];
    dados.funcao = f['value']['funcao'];
    dados.flag_cash = Number(this.flag_cash);
    dados.flag_campanha = Number(this.flag_campanha);
    dados.flag_clientes = Number(this.flag_clientes);
    dados.flag_config = Number(this.flag_config);
    dados.flag_powerbi = Number(this.flag_powerbi);
    dados.flag_relatorios = Number(this.flag_relatorios);

    console.log("dados enviados >>:"+JSON.stringify(dados));////////////////////

    // this.http.post("http://regismaicon.com.br/satsWeb/report.php", dados, {
    //   headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    // }).subscribe(data => {
    //   //console.log(data);//////////////////////
    //   if (data["id"] == 0) { // ocorreu um erro 
    //     toast("", 3000);
    //   } else {

    //   }
    // });
  
  }



}
