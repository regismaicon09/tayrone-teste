import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigurarNotificacaoComponent } from './configurar-notificacao/configurar-notificacao.component';

import { ConfiguracoesComponent } from './configuracoes.component';
import { ConfigurarPermissaoUsuarioComponent } from './configurar-permissao-usuario/configurar-permissao-usuario.component';

import { AuthGuard } from "../guards/auth.guard";

const cashbackRoutes: Routes = [
    { path: '', component: ConfiguracoesComponent },
    { path: 'configurar-notificacao', component: ConfigurarNotificacaoComponent },
    { path: 'configurar-permissoes-usuario', component: ConfigurarPermissaoUsuarioComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(cashbackRoutes)
    ], exports:[
        RouterModule
    ]
})

export class ConfiguracoesRoutingModule { }