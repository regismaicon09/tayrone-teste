import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';

import { HttpModule }  from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FocusModule } from 'angular2-focus';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { TextMaskModule } from 'angular2-text-mask';

import { ConfiguracoesRoutingModule } from './configuracoes.routing.module';

import { ConfiguracoesComponent } from './configuracoes.component';
import { ConfigurarNotificacaoComponent } from './configurar-notificacao/configurar-notificacao.component';

import { ConfigurarPermissaoUsuarioComponent } from './configurar-permissao-usuario/configurar-permissao-usuario.component';
  
@NgModule({
  imports: [
    CommonModule,
    ConfiguracoesRoutingModule,
    FormsModule,
    MaterializeModule,
    HttpModule,
    HttpClientModule,
    FocusModule,
    CurrencyMaskModule,
    TextMaskModule
  ],
  declarations: [
    ConfiguracoesComponent,
    ConfigurarNotificacaoComponent,
    
    ConfigurarPermissaoUsuarioComponent
  ], exports:[

  ]
})
export class ConfiguracoesModule { }
