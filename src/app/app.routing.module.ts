
import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PaginaNaoEncontradaComponent } from "./pagina-nao-encontrada/pagina-nao-encontrada.component";
import { LoginComponent } from './login/login.component';
import { EsquiciSenhaComponent } from './esquici-senha/esquici-senha.component';
import { MinhaContaComponent } from './minha-conta/minha-conta.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';

import { AuthGuard } from './guards/auth.guard';

import { ClientesModuleGuard } from './guards/clientes-module.guard';
import { ConfiguracoesModuleGuard } from './guards/configuracoes-module.guard';
import { RelatoriosModuleGuard } from './guards/relatorios-module.guard';

const appRoutes: Routes = [
    
    { path: 'login', component: LoginComponent },
    { path: 'esqueci-senha', component: EsquiciSenhaComponent },
    { path: 'home', component: HomeComponent },
    { path: 'relatorios', loadChildren: 'app/relatorios/relatorios.module#RelatoriosModule', canActivate: [AuthGuard, RelatoriosModuleGuard], canLoad: [AuthGuard] },
    { path: 'clientes', loadChildren: 'app/clientes/clientes.module#ClientesModule', canActivate: [AuthGuard, ClientesModuleGuard], canLoad: [AuthGuard] },
    { path: 'configuracoes', loadChildren: 'app/configuracoes/configuracoes.module#ConfiguracoesModule', canActivate: [AuthGuard, ConfiguracoesModuleGuard], canLoad: [AuthGuard] },
    { path: 'minha-conta', component: MinhaContaComponent, canActivate: [AuthGuard], canLoad: [AuthGuard] },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', component: PaginaNaoEncontradaComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: true })
    ], exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }