import { Component, OnInit, transition } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './login/auth.service';

import { Usuario } from './usuario';
import { Strings } from './strings';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  nomeApp: String;

  mostrarMenuLojista: boolean;
  showMenuLateral: boolean;
  mostrarOpcoesSearch: boolean;
  mostrarIconeMenuLateral: boolean;
  mostrarLogo: boolean;
  mostrarListaCashBack: boolean;
  mostrarListaDashBoard: boolean;
  mostrarListaConfiguracoes: boolean;

  pesquisa: string;

  usuario: Usuario = new Usuario();

  //array com os objetos que podem ser pesquisados na barra de pesquisa
  data = [
    { nome: "Configuração CashBack", link: "/configuracoes/configurar-cashback" },
    { nome: "Configuração Notificação", link: "/configuracoes/configurar-notificacao" },
    { nome: "Lançar CashBack", link: "/cashback/lancar-cashback" },
    { nome: "Resgatar CashBack", link: "/cashback/resgatar-cashback" },
    { nome: "Relatórios", link: "/relatorios" },
    { nome: "CashBack", link: "/cashback" },
    { nome: "Campanha", link: "/campanha" },
    { nome: "Home", link: "/cashback" },
    { nome: "Novo cliente", link: "/clientes/novo-cliente" },
    { nome: "Novo lojista", link: "/clientes/novo-lojista" },
    { nome: "Minha Conta", link: "/minha-conta" },
    { nome: "Meu Perfil", link: "/minha-conta" },
    //{ nome: "DashBoard Visão Geral", link: "/power-bi/visao-geral" },
   

  ];

  filteredData = [];

  contM: number;
  contE: number;

  constructor(private authService: AuthService, private router: Router) {
    this.nomeApp = Strings.nomeApp;
    this.mostrarMenuLojista = false;
    this.showMenuLateral = false;
    this.mostrarOpcoesSearch = false;
    this.mostrarIconeMenuLateral = false;
    this.mostrarListaCashBack = false;
    this.mostrarListaDashBoard = false;
    this.mostrarListaConfiguracoes = false;
    this.mostrarLogo = true;

    this.contM = 0;
    this.contE = 0;
  }

  ngOnInit() {
    $(function () { $(".button-collapse").sideNav(); }); //para funcionar o menu-lateral quando a tela for pequena (mobile)

    $(function () { $(".dropdown-button").dropdown(); }); //para mostrar o menu dropdown

    this.authService.mostraMenuLojistaEmitter.subscribe(
      mostrar => this.mostarBotoesNavBar(mostrar)
    );

    this.authService.mostraMenuLateralEmitter.subscribe(
      ml => {
        //console.log("passou pelo this.authService.mostraMenuLateralEmitter()");///////////////////////
        if (ml) {
          this.mostrarMenuLateral(4);
        } else {
          this.esconderMenuLateral();
        }
        //console.log("mudou o valor de showMenuLateral");
      }
    );

    this.authService.usuarioEmitter.subscribe(
      user => this.usuario = user
    );

    this.mostrarEsconderLogo(window.innerWidth);//dependendo do tamanho da tela não exibir a logo
  }

  mostarBotoesNavBar(m) {
    this.mostrarMenuLojista = m;
    this.mostrarEsconderLogo(window.innerWidth);
  }

  mostrarEsconderLista(tipo) {
    if (tipo == "cash") {
      this.mostrarListaCashBack = !this.mostrarListaCashBack;
    } else if (tipo == "dash") {
      this.mostrarListaDashBoard = !this.mostrarListaDashBoard;
    } else if(tipo == "conf"){
      this.mostrarListaConfiguracoes = !this.mostrarListaConfiguracoes;
    }
  }

  search(val: string) {//metodo chamado quando o usuario digita alguma coisa na barra de pesquisa
    if (!val) {
      this.filteredData = [];
    }

    if (val.length == 0) {
      this.mostrarOpcoesSearch = false;
    } else {
      this.mostrarOpcoesSearch = true;

      this.filteredData = this.data.filter(d => d.nome.toLocaleLowerCase().indexOf(val.toLocaleLowerCase()) >= 0);
    }
  }

  pesquisar() { //metodo chamado quando o usuario clica em um item da lista de pesquisa
    this.mostrarOpcoesSearch = false;
    this.pesquisa = "";
  }

  ngAfterContentChecked() {  //metodo chamado sempre que ha uma mudanca nos layouts   
    //console.log("chamou ngAfterContentChecked()");
  }

  mostrarEsconderMenuLateral() { // menu lateral que aparece quando se esta em um desktop
    //console.log("chamou a func mostrarEsconderMenuLateral(), o valor era >>:" + this.showMenuLateral);/////////////////////////////////
    this.showMenuLateral = !this.showMenuLateral;
  }

  mostrarMenuLateral(c) {// menu lateral que aparece quando se esta em um desktop
    //console.log("chamou a func mostrarMenuLateral() >>:" + c);/////////////////////////////////    
    this.showMenuLateral = true;
    if (this.contM < this.contE) {
      this.contM = this.contE + 1;
    } else {
      this.contM++;
    }
  }

  esconderMenuLateral() {// menu lateral que aparece quando se esta em um desktop
    //console.log("chamou a func esconderMenuLateral()");/////////////////////////////////    
    this.showMenuLateral = false;
    if (this.contE < this.contM) {
      this.contE = this.contM + 1;
    } else {
      this.contE++;
    }
  }

  mostrarEsconderLogo(value) {//metodo chamado quando a tela muda de tamanho
    //console.log("tamanho da tela >>:" + value);/////////////////////
    if (value <= 992) { //caiu no .m ou .s do grid
      this.esconderMenuLateral();
      this.mostrarIconeMenuLateral = false;
      this.mostrarLogo = false;
    } else {
      this.mostrarLogo = true;
      //this.mostrarMenuLateral(2);
      this.mostrarIconeMenuLateral = true;
    }
  }

  fecharMenu() {//metodo utilizado para fechar o menu lateral (mobile) que aparece quando a tela é pequena
    $(function () {
      $(".button-collapse").sideNav('hide');
    });
    this.mostrarListaCashBack = false;
  }

  voltar() { //metodo chamado quando o usuario clica na lodo do sistema que fica na navbar 
    if (this.authService.getLojistaAutenticado() == true) {
      this.router.navigate(['home']);
      this.mostrarMenuLateral(3);
    } else {
      this.fazerLogout();
    }
  }

  fazerLogout() { //metodo chamado quando o usuario clica na opcao logout na navBar
    this.authService.fazerLogoutLojista();
  }

}
